#include "mbed.h"
#include "TextLCD.h"

DigitalIn userButton(USER_BUTTON);

TextLCD lcd(PA_0, PA_1, PA_4, PB_0, PC_1, PC_0); // RS, E, D4-D7

Timer t;

typedef struct time_s {
   int cents;
   int minutes;
   int seconds;
} time_s;

time_s get_mm_ss_ms_format(int ms){
    time_s read;
    div_t qr = div(ms, 1000);
    read.cents = qr.rem % 100;    
    qr = div(qr.quot,60);
    read.seconds = qr.rem;    
    qr = div(qr.quot,60);
    read.minutes = qr.rem;
    
    return read;    
}

int main() {
    int state = 0;
    lcd.cls();
        
    while (1) {
        if (userButton == 0) {
            switch (state)
            {
            case 1 : 
                t.stop();
                state = 2;
                break;
            case 2:
            t.reset();
            state = 0;
            break;

            default:
            t.start();
            state = 1;
                break;
            }
            wait(0.5);
        }
        int read = t.read_ms(); 
        time_s time = get_mm_ss_ms_format(read);
        
        lcd.locate(0,0);
        lcd.printf("Time = %02d:%02d:%02d",time.minutes,time.seconds,time.cents);
    }
}
